from app.extensions import db

from flask_login import UserMixin
from flask_login import AnonymousUserMixin


class ApiUser(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(), nullable=False)
    is_admin = db.Column(db.Boolean(), default=False)

    @property
    def is_authenticated(self):
        return True


class AnonymousUser(AnonymousUserMixin):
    is_admin = False

