from sqlalchemy.dialects.postgresql import UUID
from app.extensions import db
from app.models.dfs_operation.utils import TimestampMixin


class User(TimestampMixin, db.Model):
    id = db.Column(UUID(as_uuid=True), primary_key=True)
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    email = db.Column(db.String(120), nullable=False, unique=True)
    hidden = db.Column(db.Boolean, default=False)
    locked = db.Column(db.Boolean, default=False)
    language = db.Column(db.String(5), default='en')
    phone = db.Column(db.String(32))
    activated = db.Column(db.Boolean, default=True)

    @property
    def full_name(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    def __repr__(self):
        return "[<{0}> <{1}>]".format(self.full_name, self.email)

class Manager(User):
    id = db.Column(db.ForeignKey('user.id'), primary_key=True)
