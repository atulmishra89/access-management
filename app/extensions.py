"""
@module extensions
This module implements third party packages for the application.
Initialized in the extensions module. All the instance are import to the factory method
and registered for the usage the specified application method.
"""

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_principal import Principal
from flask_cors import CORS
from flask_babel import Babel



db = SQLAlchemy()
login_manager = LoginManager()
principal = Principal()
cors = CORS(resources={r"*": {"origins": "*"}})
babel = Babel()
