from flask import Flask
from flask import request

from app.extensions import db
from app.extensions import login_manager
from app.extensions import principal
from app.extensions import babel

from flask_potion import Api
from flask_login import login_required
from flask_potion.contrib.principals import principals
from flask_potion.contrib.alchemy import SQLAlchemyManager


def dfs_operation_app(config):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config)

    db.init_app(app)
    login_manager.init_app(app)
    principal.init_app(app)
    babel.init_app(app)

    @babel.localeselector
    def get_locale():
        return request.accept_languages.best_match(app.config['LANGUAGES'])

    api = Api(app, prefix='/dfs_operation', decorators=[],
              default_manager=principals(SQLAlchemyManager))

    from app.resources.dfs_operation.users import UserResource
    from app.resources.dfs_operation.users import ManagerResource
    api.add_resource(UserResource)
    api.add_resource(ManagerResource)


    with app.app_context():
        db.drop_all()
        db.create_all()

    return app
