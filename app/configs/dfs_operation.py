
class BaseConfig:
    LANGUAGES = ['en']
    BABEL_DEFAULT_LOCALE = 'en'

class DevConfig(BaseConfig):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "postgres://{user}:{password}@{host}/{db}".format(
        user="john",
        password="mysql",
        host="localhost",
        db="dev_dfs_dd"
    )

class TestConfig(DevConfig):
    DEBUG = True
    TESTING = True


class ProdConfig(DevConfig):
    DEBUG = False


api_config = {
    'dev': DevConfig,
    'test': TestConfig,
    'prod': ProdConfig
}