from flask_potion import ModelResource
from flask_potion.routes import Route
from flask_potion import fields
from flask_babel import gettext

from app.models.dfs_operation.users import User
from app.models.dfs_operation.users import Manager


class UserResource(ModelResource):
    class Meta:
        model = User


class ManagerResource(ModelResource):
    class Meta:
        model = Manager

