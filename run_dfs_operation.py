from app.main import dfs_operation_app
from app.configs.dfs_operation import api_config


app = dfs_operation_app(api_config['dev'])

if __name__ == '__main__':
    app.run(host="0.0.0.0")
